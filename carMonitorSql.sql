CREATE TABLE t_car_order_link(
			CAR_ID varchar(100)
			,ORDER_INDEX varchar(100)
			,PRIMARY KEY(CAR_ID,ORDER_INDEX)
);
select * from t_car_order_link where CAR_ID = '' and ORDER_INDEX = ''
insert into t_car_order_link(CAR_ID,ORDER_INDEX) values('','');
delete from t_car_order_link where CAR_ID = '' and ORDER_INDEX = ''

CREATE TABLE t_order_info(
			ORDER_INDEX varchar(100)
			,ORDER_STATE varchar(100)
			,PRIMARY KEY(ORDER_INDEX)
);

select ORDER_INDEX as order_index,ORDER_STATE as order_state from t_order_info where ORDER_INDEX = ''
insert into t_order_info(ORDER_INDEX,ORDER_STATE) values('','');
update t_order_info set ORDER_STATE = '' where ORDER_INDEX = '';

CREATE TABLE T_CARPOINTS (
			 CAR_ID 			varchar(100)
			,CREATE_TIME 		varchar(100)
			,LATITUDE 			varchar(100)
			,LONGITUDE 			varchar(100)
			,PRIMARY KEY(CAR_ID,CREATE_TIME)
);
--sqlserver
select top 1 CAR_ID ,CREATE_TIME ,LATITUDE,LONGITUDE from T_CARPOINTS	order by CREATE_TIME desc
--sqlite
select  CAR_ID ,CREATE_TIME ,LATITUDE,LONGITUDE from T_CARPOINTS	order by CREATE_TIME desc limit 0,1

CREATE TABLE T_CAR_INFO (
			 CAR_ID 			varchar(100)
			,VIHICLE_TYPE 		varchar(100) 
			,BUY_TIME 			varchar(32) 
			,CAR_STATE 			varchar(32) default '正常'
			,PRIMARY KEY(CAR_ID)
);
select CAR_ID,VIHICLE_TYPE,BUY_TIME,CAR_STATE from T_CAR_INFO 
insert  into T_CAR_INFO(CAR_ID,VIHICLE_TYPE,BUY_TIME) values('J001','普通轿车',(date('now')));

CREATE TABLE THINK_USER (
		  ACCOUNT 			varchar(100) 	NOT NULL,
		  NICKNAME 			varchar(100) 	NOT NULL,
		  PASSWORD 			varchar(32) 	NOT NULL,
		  REMARK 			varchar(255)  	NULL,
		  EMAIL 			varchar(100) 	NULL,
		  PRIMARY KEY  (ACCOUNT)
) ;
13
CREATE TABLE THINK_ROLE (
			 NAME 			varchar(100)
			,STATUS 		int
			,REMARK 		varchar(255) NULL
			,PRIMARY KEY  (NAME)
) ;
14
CREATE TABLE THINK_ROLEUSER (
			 ROLE_NAME 		varchar(100)
			,USER_ACOUNT 	varchar(100)
			,primary key(ROLE_NAME,USER_ACOUNT)
) ;

insert into THINK_ROLE(NAME,REMARK) values('editor','高级用户') ;
insert into THINK_ROLE(NAME,REMARK) values('viewer','普通用户') ;
insert into THINK_ROLE(NAME,REMARK) values('adminer','系统管理员') ;

insert into THINK_USER(ACCOUNT,NICKNAME,PASSWORD) values('admin','管理员','202cb962ac59075b964b07152d234b70') ;
insert into THINK_USER(ACCOUNT,NICKNAME,PASSWORD) values('leader','高级用户','202cb962ac59075b964b07152d234b70'); 
insert into THINK_USER(ACCOUNT,NICKNAME,PASSWORD) values('demo','普通用户','202cb962ac59075b964b07152d234b70') ;

insert into THINK_ROLEUSER(ROLE_NAME,USER_ACOUNT) values('editor','leader');
insert into THINK_ROLEUSER(ROLE_NAME,USER_ACOUNT) values('viewer','demo');
insert into THINK_ROLEUSER(ROLE_NAME,USER_ACOUNT) values('adminer','admin');